# Gets TCGPlayer CSV and outputs quantity + Scryfall ID for Manabox
import tkinter as tk
from tkinter import filedialog
import requests
from pathlib import Path
import json
from dateutil import parser
import os

csv_columns = ["Quantity", "Product ID", "Name", "Rarity"]

def urlretrieve(url, filepath):
    r = requests.get(url)
    with open(filepath, "wb") as outfile:
        outfile.write(r.content)

def csv_split(line):
    result = []
    current_str = ""
    include_comma = False
    for char in line:
        if char == "\"":
            include_comma = not include_comma
            continue
        if char == "," and not include_comma:
            result.append(current_str)
            current_str = ""
            continue
        current_str += char
    return result

def write_csv(split):
    result = []
    for entry in split:
        if "," in entry:
            result.append("\"" + entry + "\"")
        result.append(entry)
    return ",".join(result)

def csv_to_dict(csv_file_path):
    with open(csv_file_path, "r") as f:
        csv_str = f.read()
    split = csv_str.split("\n")
    columns = split[0].split(",")
    columns_to_find = csv_columns
    column_result = {}
    for i, column in enumerate(columns):
        for column_to_find in columns_to_find:
            if column == column_to_find:
                if column_to_find not in column_result:
                    column_result[column_to_find] = {}
                column_result[column_to_find]["index"] = i
    for column_to_find in columns_to_find:
        if column_to_find not in column_result:
            raise ValueError(f"Missing {column_to_find} column")

    result = {}
    key_column = "Product ID"
    for i in range(1, len(split)):
        line = csv_split(split[i])

        result[line[column_result[key_column]["index"]]] = {}
        for column, obj in column_result.items():
            result[line[column_result[key_column]["index"]]][column] = line[obj["index"]]

    return result

def all_identifiers_to_dict(needs_update):
    if needs_update:
        print("Downloading AllIdentifiers.json from MTGJson...")
        urlretrieve("https://mtgjson.com/api/v5/AllIdentifiers.json", "AllIdentifiers.json")

    if not Path("AllIdentifiersByProductID.json").exists() or needs_update:
        print("Writing AllIdentifiers cache...")
        with open("AllIdentifiers.json", "r", encoding="utf-8") as f:
            all_identifiers = json.loads(f.read())
        result = {}
        for obj in all_identifiers["data"].values():
            if "tcgplayerProductId" in obj["identifiers"]:
                result[obj["identifiers"]["tcgplayerProductId"]] = {
                    "Scryfall ID": obj["identifiers"]["scryfallId"]
                }
        with open("AllIdentifiersByProductID.json", "w", encoding="utf-8") as f:
            f.write(json.dumps(result))
    else:
        with open("AllIdentifiersByProductID.json", "r", encoding="utf-8") as f:
            result = json.loads(f.read())
    return result

def output_file(out_file_path, out_missing_cards_path, new_card_dict, all_identifiers_dict):
    print("Writing Manabox csv...")
    result_str = ""
    columns = ["Quantity", "Scryfall ID"]
    result_str += ",".join(columns) + ",\n"
    missing_str = ",".join(csv_columns) + ",\n"
    has_missing = False
    for product_id, obj in new_card_dict.items():
        if product_id not in all_identifiers_dict:
            has_missing = True
            if obj["Rarity"] != "Token":
                print(f"ALERT: Failed to find non-Token MTGJson entry for {obj['Name']}, TCGPlayer Product ID {obj['Product ID']}")
            missing_str += write_csv([obj[column] for column in csv_columns]) + ",\n"
        else:
            column_vals = {
                "Quantity": obj["Quantity"],
                "Scryfall ID": all_identifiers_dict[product_id]["Scryfall ID"]
            }
            result_str += ",".join([column_vals[column] for column in columns]) + ",\n"
    with open(out_file_path, "w") as f:
        print(f"Wrote result out to {out_file_path}")
        f.write(result_str)
    if has_missing:
        with open(out_missing_cards_path, "w") as f:
            print(f"Some cards could not be found in MTGJson. A CSV of them has been written to {out_missing_cards_path}")
            f.write(missing_str)

def get_meta():
    urlretrieve("https://mtgjson.com/api/v5/Meta.json", "_meta.json")
    with open("_meta.json", "r") as f:
        new_meta = json.loads(f.read())
    needs_update = False
    if Path("Meta.json").exists():
        with open("Meta.json", "r") as f:
            current_meta = json.loads(f.read())
            if parser.parse(new_meta["data"]["date"]) > parser.parse(current_meta["data"]["date"]):
                needs_update = True
    else:
        needs_update = True
    os.replace("_meta.json", "Meta.json")
    return needs_update

def main():
    root = tk.Tk()
    root.withdraw()

    csv_file_path = filedialog.askopenfilename()
    new_card_dict = csv_to_dict(csv_file_path)

    needs_update = get_meta()

    all_identifiers_dict = all_identifiers_to_dict(needs_update)

    csv_file_path_obj = Path(csv_file_path)
    new_path = csv_file_path_obj.parent / (csv_file_path_obj.stem + "_manabox.csv")
    missing_cards_path = csv_file_path_obj.parent / (csv_file_path_obj.stem + "_missing_cards.csv")
    output_file(new_path, missing_cards_path, new_card_dict, all_identifiers_dict)

if __name__ == "__main__":
    main()